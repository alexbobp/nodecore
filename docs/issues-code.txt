========================================================================
ISSUES-CODE: Issues related to code quality and maintainability
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Deprecate air_equivalent property and move to a group
	- This is probably going to be deprecated by the engine

- Would be nice to finally have a universal "active thing modifier" that
  combines ABMs and AISMs instead of having to have both.
	- Could also maybe try to use DNTs, e.g. have a single DNT
	  patrol ABM for everything and every other interaction would
	  be node timer managed?

- Standardize some node metadata publishing
	- Colors.txt for minetest mapper from node registrations

- Need to standardize storage API between storeboxes and bare stacks
	- Storebox API is a mess anyway and needs a lot of cleanup.

- If DNTs are deferred due to either stasis or arealoaded, need to
  set the timer re-firing to some sane future time.

- Vector reform: use actual vector.new instead of manual table
  construction everywhere possible.

- Consider modernizing translation pipeline
	- Use JSON instead of po format for easier parsing?
	- Follow Piranesi Restoration Project as an example.
	- https://discord.com/channels/369122544273588224/369123175583186964/1037043979209936896

- Unified heat API?
	- Unify quenching and heating
	- Replace quenched, touchgroups, neargroups

- Gametime handling improvements.
	- Need to be robust in event of gametime resetting to zero,
	  e.g. env_meta.txt corruption.
	- Probably need to separate nodecore gametime entirely from
	  minetest gametime.
	- Always need to guard against the backward flow of time
	  in normal gameplay circumstances, e.g. due to loss of
	  meta files.
		- Need to store backup in world dir next to
		  env_meta.txt.
		- Partial world resets (e.g. nuking map, keeping
		  dais ex machina) should not reset time.
	- Allow customizing time flow in vanilla:
		- Time rate multipliers, default 1, for each of:
			- Server offline (os.time() adjust)
			- Server online but empty (no players online)
			- Normal gameplay
		- Probably need to track accumulators for each
		  separately in mod storage and then add them together
		  to get gametime.

- Replace digparticles with "node" and maybe "node_tile" options for
  particlespawners new in 5.3+
	- N.B. the docs imply it only works for nodes, not items,
	  so tool breakage particles may still require the logic.

- Optimize sponge colony growth logic
	- For each sponge found, push into a queue
	- Process queue on step:
		- Skip all "seen" sponges
		- For each sponge, floodfill to find colony
		- Mark all sponges in colony as "seen"
	- Look for a "colony center" sponge that has colony metadata
		- Process growth for entire colony, generate sponges
			as needed
		- If a sponge is dug, check to see if it's the colony
			seed and has the metadata
		- Transfer metadata to any face-touching sponge

- Recipe "rate adjust" is messed up.
	- Make rate_adjust ONLY use config
	- Add a speed adjustment for manual changes, e.g. when
	  tool speed is used

- Soaking API improvmements
	- Simpler version of the soaking API that uses an exponential random
	  distribution and mean time between events to determine if at least one
	  event happened
		- Support cascading, i.e. pushing time into another event type
		for replacement nodes?
	- Better altnernative based on lantern charging that reduces
	  metadata churn (only update meta if we cross some threshold instead
	  of every check)

- LookTips
	- API needs an overhaul
		- Use a single generic name everywhere (getdesc?)
		- Simplify calling, maybe pass data table
		- Unify between node/itemdefs, entdefs, etc.
	- Update mods depending on touchtips

- Use set_local_animation again?
	- Apparently it may make sense instead of handling every animation on the
	  server side, to send sets of animations for the player based on context,
	  e.g. swap between "swimup", "swimdown" and "standing/walking" sets and
	  let the client handle the parts it can handle.
	https://github.com/minetest/minetest_game/pull/2917/files#diff-bb0f88d32425a778a80208ad343f9e94880cba87893232410d8c0fba22295343R129

- Update based on recentish engine sound improvements
	- Improvements committed 2020-02-01
	- exclude_player in sound_play instead of looping through
	  https://github.com/minetest/minetest/commit/ace3c7611
	- Remove NC's "social sounds" feature for dig/place
	  https://github.com/minetest/minetest/commit/c10e18859

- Expand stackapply recipes
	- Allow them to work with non-stacks?
		- Hook into item_place
	- Honor normal and other attributes?
	- Work for mixing coal into wet concrete?
	- Make stickcraft recipes work in tight spaces?
		- Simulate node placement for place recipes?

- General Architectural Audit
	- Convert more things to autostart DNTs?
	- Anywhere we are using explicit names or lists of names in game, we
	  should probably try registering groups to allow future expansion
	- Transactional api ref localization
		- For certain large computations, like an optic processing, it may be
		  worthwhile to capture some often-repeated functions like minetest.get_node
		  as locals.
		- May need to test performance on a non-JIT build, where this is expected
		  to have a larger impact, since JIT probably optimizes the difference away.

- Unify the "node destruction particles" system.  Allow complex options,
  like alternative node def, particle def, disable on silk touch.

- Organize into modpacks for layers
	- Compat
	- Util
	- API
	- Natural
	- Tech

- Try to detect and separate out translation strings coming from
  non-core mods so we can consistently generate the game translation
  file without having to disable mods.

- Automate uploading translation source strings?

- Flammability ABM ordering.  Always do extinguish check after ignite
  checks or vice versa, so suboptimal firestarting is consistent

- Overhaul player health to work entirely virtual
	- Raise max up a lot to make room for fine grained falling
	  damage so we can raise threshold
	- Consider falling damage add percent to all nodes?
	- Add API for doing fake damage effect using set hp

- Consider full eversion of event handlers
	- Disallow (or automatically convert) traditional single-handlers
	  on all definitions
	- Use a register_on_something() method to hook pretty much
	  everything

- Unify mkreg
	- Add validation/modifying and order comparison funcs
	- Use for recipes, mapgen_shared, playerstep, etc.
	- Consider replacing with register_generic():
		- Allow lazy registration, i.e. a register method that can
		  register for things not yet defined, and allow later registration
		  of the registration definition that will pick up those missed.
	- Define order for inv tabs

- Some time-integral processes are not resetting when prereqs removed
	- Cooking resumes instead of resetting when fire removed.
	- Leaching resumes instead of resetting when water removed.

- Make leaf decay group-driven for mod applicability.

- Switch some extended attributes to first-class support instead of modifying
  definition at registration time.
	- e.g. the way drop_in_place used to be an after_dig_node hook
	  modifier but became first-class.

- Add a nodecore.inherit_node() helper that registers a node as a
  modified copy of another definition, e.g. for planted eggcorns copying
  dirt, door panels copying original materials, etc.

- Unify sponge growth logic so it can be used for mapgen, to ensure
  that spawned sponges match naturally-grown ones?

- Easier recipe API:
	- Use ascii art like ezschematic?
	- Aliases like "sides"/"corners"?
	- Make larger/complex recipes easier to define.

- API for recursing through embedded inventories, e.g. in totes.
	- Lux reaction cannot sense other lux inside totes.

- Scripts to validate dependency graph.
	- Tag deps directly in code at place of use.
	- Auto-generate mod.conf / depends.txt

- API Cleanup
	- Further nc_api break-up, clean up util functions.
	- Heat API
		- Quenched, Flames as numbers or nil?
	- Unify nc_items and visinv API.
		- Stack nodes are "special", get first-class support.
	- APIs for different kinds of "neighbor" scans
		- Face, edge, corner touching

........................................................................
========================================================================
