return {
	de = 595,
	el = 8,
	en = 606,
	eo = 76,
	es = 166,
	es_US = 166,
	fr = 575,
	ia = 0,
	it = 184,
	lzh = 1,
	pl = 1,
	pt = 268,
	pt_BR = 268,
	ru = 606,
	sk = 147,
	tok = 17,
	uk = 25,
	zh = 302,
	zh_Hans = 302,
	zh_Hant = 302,
}
